# How does it work

- Create a merge request
- Edit in `versions` the version number of the toolkit
- Push and wait for the pipeline

## Image Names

- `{IMAGENAME}:{MR-ID}` (Test image)
- `{IMAGENAME}:{VERSION}` (Specific version image)
- `{IMAGENAME}:latest` (Latest release image)